# nudge

## Usage

```
GITLAB_API_PRIVATE_TOKEN=YOUR_TOKEN exe/nudge status
```

```
Which milestone would you like to check? 13.2
Stage Label? group::continuous integration
Searching www-gitlab-com for group group::continuous integration
Processing [============================================================]
Stale Issues for 13.2
┌────────────────────────────────────────────────────────────────┬───────────────────────────────────────────────────────────────┬──────────────┐
│  Issue Title (9)                                               │  Issue URL                                                    │  Assignees   │
├────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────────────┼──────────────┤
│  Log noise "zip archive format invalid, code: 10" is seen for  │  https://gitlab.com/gitlab-org/gitlab-workhorse/-/issues/272  │  [REDACTED]  │
│  Artifact storage does not seem to be correct                  │  https://gitlab.com/gitlab-org/gitlab/-/issues/223034         │  [REDACTED]  │
│  UI to browse locked artifacts                                 │  https://gitlab.com/gitlab-org/gitlab/-/issues/217511         │  [REDACTED]  │
│  CI linter: Extend errors messaging to include warnings (MVC)  │  https://gitlab.com/gitlab-org/gitlab/-/issues/216444         │  [REDACTED]  │
│  Stored XSS on the job page                                    │  https://gitlab.com/gitlab-org/gitlab/-/issues/215538         │  [REDACTED]  │
│  Efficient counters                                            │  https://gitlab.com/gitlab-org/gitlab/-/issues/24469          │  [REDACTED]  │
│  Keep latest artifacts for the last successful jobs            │  https://gitlab.com/gitlab-org/gitlab/-/issues/16267          │  [REDACTED]  │
│  Make it possible to define a cartesian product/matrix for bu  │  https://gitlab.com/gitlab-org/gitlab/-/issues/15356          │  [REDACTED]  │
│  Show build status in branch list                              │  https://gitlab.com/gitlab-org/gitlab/-/issues/14124          │  [REDACTED]  │
└────────────────────────────────────────────────────────────────┴───────────────────────────────────────────────────────────────┴──────────────┘
Unassigned Issues for 13.2
┌────────────────────────────────────────────────────────────────┬────────────────────────────────────────────────────────┬──────────────┐
│  Issue Title (23)                                              │  Issue URL                                             │  Assignees   │
├────────────────────────────────────────────────────────────────┼────────────────────────────────────────────────────────┼──────────────┤
│  Problem of introducing a breaking change in gitlab-ci.yml     │  https://gitlab.com/gitlab-org/gitlab/-/issues/227022  │  [REDACTED]  │
│  POC for making pipeline permissions more controllable and fl  │  https://gitlab.com/gitlab-org/gitlab/-/issues/221192  │  [REDACTED]  │
│  Gitlab::Config::Entry::ComposableHash (Follow-up from "Imple  │  https://gitlab.com/gitlab-org/gitlab/-/issues/220759  │  [REDACTED]  │
│  CI 13.2 Needs Weight/Research                                 │  https://gitlab.com/gitlab-org/gitlab/-/issues/220532  │  [REDACTED]  │
│  Use `danger` for `Merge` button if pipeline has been skipped  │  https://gitlab.com/gitlab-org/gitlab/-/issues/220306  │  [REDACTED]  │
│  Remove `ci_atomic_processing` and `ci_composite_status` feat  │  https://gitlab.com/gitlab-org/gitlab/-/issues/218536  │  [REDACTED]  │
│  Failure in qa/specs/features/browser_ui/4_verify/pipeline/cr  │  https://gitlab.com/gitlab-org/gitlab/-/issues/217606  │  [REDACTED]  │
│  Custom help text on Run Pipeline form                         │  https://gitlab.com/gitlab-org/gitlab/-/issues/215415  │  [REDACTED]  │
│  Artifacts should be served with text/plain Content-Type       │  https://gitlab.com/gitlab-org/gitlab/-/issues/214707  │  [REDACTED]  │
│  Make the BuildHooksWorker idempotent                          │  https://gitlab.com/gitlab-org/gitlab/-/issues/216562  │  [REDACTED]  │
│  Docs feedback: Usage of caches / artifacts for Maven target   │  https://gitlab.com/gitlab-org/gitlab/-/issues/212943  │  [REDACTED]  │
│  Pipeline triggers do not operate in a predictable and unders  │  https://gitlab.com/gitlab-org/gitlab/-/issues/212784  │  [REDACTED]  │
│  Explore pipeline graph UX when more than 2 upstream or downs  │  https://gitlab.com/gitlab-org/gitlab/-/issues/212598  │  [REDACTED]  │
│  Some jobs claim to upload artifacts but artifacts aren't act  │  https://gitlab.com/gitlab-org/gitlab/-/issues/212349  │  [REDACTED]  │
│  Every successful pipeline should have a started_at value      │  https://gitlab.com/gitlab-org/gitlab/-/issues/210353  │  [REDACTED]  │
│  GitLab pipeline job retry trigger associated correctly but s  │  https://gitlab.com/gitlab-org/gitlab/-/issues/202082  │  [REDACTED]  │
│  Child pipeline artifacts cannot be downloaded via ref-based   │  https://gitlab.com/gitlab-org/gitlab/-/issues/201784  │  [REDACTED]  │
│  Maintainer can execute scheduled CI pipeline as another user  │  https://gitlab.com/gitlab-org/gitlab/-/issues/118782  │  [REDACTED]  │
│  Pipelines - .* misbehaves when pattern matching against a va  │  https://gitlab.com/gitlab-org/gitlab/-/issues/35438   │  [REDACTED]  │
│  Merge Train text helper is misleading for Merge Immediately   │  https://gitlab.com/gitlab-org/gitlab/-/issues/12997   │  [REDACTED]  │
│  MR with no commits does not show triggered pipeline           │  https://gitlab.com/gitlab-org/gitlab/-/issues/24059   │  [REDACTED]  │
│  Additional improvements to mirror user and pipeline build se  │  https://gitlab.com/gitlab-org/gitlab/-/issues/2984    │  [REDACTED]  │
│  Ensure after_script is called for cancelled and timed out pi  │  https://gitlab.com/gitlab-org/gitlab/-/issues/15603   │  [REDACTED]  │
└────────────────────────────────────────────────────────────────┴────────────────────────────────────────────────────────┴──────────────┘
```

