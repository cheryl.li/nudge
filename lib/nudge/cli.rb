# frozen_string_literal: true

require 'thor'

module Nudge
  # Handle the application command line parsing
  # and the dispatch to various command objects
  #
  # @api public
  class CLI < Thor
    # Error raised by this runner
    Error = Class.new(StandardError)

    desc 'version', 'nudge version'
    def version
      require_relative 'version'
      puts "v#{Nudge::VERSION}"
    end
    map %w(--version -v) => :version

    desc 'status', 'Command description...'
    method_option :help, aliases: '-h', type: :boolean,
                         desc: 'Display usage information'
    def status(*)
      if options[:help]
        invoke :help, ['status']
      else
        require_relative 'commands/status'
        Releasepost::Commands::Status.new(options).execute
      end
    end
  end
end
