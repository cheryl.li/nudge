# frozen_string_literal: true

require 'memoist'
require 'gitlab'
require 'http'
require 'tty-progressbar'
require 'tty-table'
require 'pry'

CLIENT = Gitlab.client(endpoint: 'https://gitlab.com/api/v4')

class Finder
  extend Memoist

  attr_reader :milestone, :labels, :group_id

  def initialize(milestone='', labels = '', group_id = 9970)
    @milestone        = milestone
    @labels           = labels # "release post, #{labels}"
    @group_id         = group_id
    @release_post_mrs = []
  end

  def start_progress_bar(total)
    @progress_bar = TTY::ProgressBar.new(
      "Processing [:bar]",
      total: total,
      width: 60
    )
  end

  def unassigned_issues
    search = CLIENT.group_issues(@group_id, {milestone: @milestone, labels: @labels, state: 'opened', assignee_id: 'None'})
    issues = search.auto_paginate
    issues
  end
  memoize :unassigned_issues

  def assigned_issues
    search = CLIENT.group_issues(@group_id, {milestone: @milestone, labels: @labels, state: 'opened', assignee_id: 'Any'})
    issues = search.auto_paginate
    issues
  end
  memoize :assigned_issues

  def issues_with_stale_mrs    
    stale_issues = []

    assigned_issues.each do |issue|
      @progress_bar.advance(1)
      stale_issues << issue if has_stale_mrs?(issue)
    end
  
    stale_issues
  end
  memoize :issues_with_stale_mrs

  def run!
    start_progress_bar(assigned_issues.count)

    render_output(issues_with_stale_mrs, "Stale Issues for #{@milestone}")

    render_output(unassigned_issues, "Unassigned Issues for #{@milestone}")
  end

  def assignee_names(assignees)
    assignees.map{|a| a.name}.join(', ')
  end

  def render_output(issues, title)
    rows = []

    issues.each do |issue|
      rows << [issue.title[0,60], issue.web_url, assignee_names(issue.assignees)]
    end

    table = TTY::Table.new(["Issue Title (#{rows.count})", 'Issue URL', 'Assignees'], rows)

    pastel = Pastel.new
    puts pastel.red(title)

    puts table.render(:unicode, padding: [0,2], multiline: false)
  end

  def has_stale_mrs?(issue)
    # related_merge_requests API not supported in Gitlab gem
    url = "https://gitlab.com/api/v4/projects/#{issue.project_id}/issues/#{issue.iid}/related_merge_requests"
    response = HTTP.headers("PRIVATE-TOKEN" => ENV['GITLAB_API_PRIVATE_TOKEN']).get(url)
    related_mrs = JSON.parse(response)

    related_mrs.each do |related_mr|
      next unless related_mr["milestone"] && related_mr["milestone"]["title"] == @milestone && related_mr["state"] == 'opened' 
      return true if Time.parse(related_mr["updated_at"]) < (Time.now - 86400)
    end

    return false
  end
end
